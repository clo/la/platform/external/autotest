# Copyright (c) 2021 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = "ChromeOS Team"
NAME = "firmware_MiniosPriority.minios_a"
PURPOSE = "Set the MiniOS priority and verify the device can boot to MiniOS in any priority setting"
CRITERIA = "This test will fail if the device fail to boot to MiniOS or boot to the wrong partition"
ATTRIBUTES = "suite:faft_bios, suite:faft_bios_ro_qual, suite:faft_lv4, suite:faft_bios_tot"
DEPENDENCIES = "servo_state:WORKING"
TIME = "MEDIUM"
TEST_CATEGORY = "Functional"
TEST_CLASS = "firmware"
TEST_TYPE = "server"
JOB_RETRIES = 0
PY_VERSION = 3

DOC = """
This test requires the device support MiniOS. At runtime, this test uses the
crossystem tool to modify the MiniOS priority and try to boot MiniOS from
firmware manual recovery screen. After booting, this test will verify if the
device successfully boot to the MiniOS. This test does not cover verifying
if the device successfully boots to the specified partition.
"""

args_dict = utils.args_to_dict(args)
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)

def run(machine):
    host = hosts.create_host(machine, servo_args=servo_args)
    job.run_test("firmware_MiniosPriority", host=host, cmdline_args=args,
                 disable_sysinfo=True, minios_priority='a')

parallel_simple(run, machines)
