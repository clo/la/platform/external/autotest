# Copyright 2021 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = 'chromeos-bluetooth'
NAME = 'bluetooth_AdapterAUHealth.au_a2dp_playback_back2back_test'
PURPOSE = ('Run the A2DP playback stream back2back test')
CRITERIA = 'Pass all audio chunk checking'
ATTRIBUTES = 'suite:bluetooth, suite:bluetooth_e2e'
TIME = 'SHORT'  # Approximately 3 mins
TEST_CATEGORY = 'Functional'
TEST_CLASS = 'bluetooth'
TEST_TYPE = 'server'
DEPENDENCIES = 'bluetooth, working_bluetooth_btpeer:1'
PY_VERSION = 3

DOC = """
    To run the A2DP playback stream back to back test.

    This test repeats to start and stop the playback stream and verify that
    the Bluetooth device receives the stream correctly.
    """

args_dict = utils.args_to_dict(args)

def run(machine):
    host = hosts.create_host(machine)
    job.run_test('bluetooth_AdapterAUHealth', host=host, num_iterations=1,
                 args_dict=args_dict, test_name=NAME.split('.')[1])

parallel_simple(run, machines)
