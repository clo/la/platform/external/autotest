# Copyright 2017 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = "ChromeOS Team"
NAME = "firmware_PDProtocol"
PURPOSE = "Verify PD protocol negotiation."
CRITERIA = "This test will fail if PD is negotiated when running from USB image."
DEPENDENCIES = "servo_state:WORKING, servo_usb_state:NORMAL"
TIME = "SHORT"
TEST_CATEGORY = "Functional"
TEST_CLASS = "firmware"
TEST_TYPE = "server"
JOB_RETRIES = 0
PY_VERSION = 3

DOC = """
This test checks:
  - That the DUT negotiated PD in dev mode from SSD boot
  - That the DUT did not negotiate PD when booted from test image from recovery
"""

args_dict = utils.args_to_dict(args)
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)
pdtester_args = hosts.CrosHost.get_pdtester_arguments(args_dict)

def run(machine):
    host = hosts.create_host(machine, servo_args=servo_args,
                             pdtester_args=pdtester_args)
    job.run_test("firmware_PDProtocol", host=host, cmdline_args=args,
                 disable_sysinfo=True)

parallel_simple(run, machines)
