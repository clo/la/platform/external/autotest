# Copyright (c) 2012 The Chromium OS Authors. All rights reserved.
# Use of this source code is governed by a BSD-style license that can be
# found in the LICENSE file.

from autotest_lib.server import utils

AUTHOR = "ChromeOS Team"
NAME = "firmware_InvalidUSB"
PURPOSE = "Servo based booting an invalid USB image test"
CRITERIA = "This test will fail if the invalid USB boots successfully"
ATTRIBUTES = "suite:faft_bios, suite:faft_bios_ro_qual, suite:faft_bios_rw_qual, suite:faft_lv4, suite:faft_normal, suite:faft_bios_tot"
DEPENDENCIES = "servo_state:WORKING, servo_usb_state:NORMAL"
TIME = "SHORT"
TEST_CATEGORY = "Functional"
TEST_CLASS = "firmware"
TEST_TYPE = "server"
JOB_RETRIES = 0
PY_VERSION = 3

DOC = """
This test requires a USB disk plugged-in, which contains a ChromeOS test
image (built by "build_image --test"). On runtime, this test corrupts the
USB image and tries to boot into it. A failure is expected. It then
restores the USB image and boots into it again.
"""

args_dict = utils.args_to_dict(args)
servo_args = hosts.CrosHost.get_servo_arguments(args_dict)

def run_invalidusb(machine):
    host = hosts.create_host(machine, servo_args=servo_args)
    job.run_test("firmware_InvalidUSB", host=host, cmdline_args=args,
                 disable_sysinfo=True, dev_mode=False, tag="normal")

parallel_simple(run_invalidusb, machines)
