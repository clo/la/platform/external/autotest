# https://gitlab.freedesktop.org/mesa/mesa/-/issues/4641
dEQP-VK.ssbo.phys.layout.random.16bit.scalar.13

dEQP-VK.binding_model.descriptorset_random.sets8.constant.ubolimitlow.sbolimithigh.imglimithigh.iublimithigh.uab.frag.0
dEQP-VK.binding_model.descriptorset_random.sets8.unifindexed.ubolimitlow.sbolimithigh.imglimithigh.iublimitlow.uab.frag.0
dEQP-VK.binding_model.descriptorset_random.sets8.unifindexed.ubolimitlow.sbolimithigh.imglimithigh.iublimithigh.uab.frag.0
dEQP-VK.binding_model.descriptorset_random.sets8.unifindexed.ubolimitlow.sbolimithigh.imglimithigh.noiub.uab.frag.0

# These tests should not exist. They are from renderpass-with-dynamic-rendering.txt which should
# have been removed upstream.
dEQP-VK.renderpass_with_dynamic_rendering.dedicated_allocation.*
dEQP-VK.renderpass_with_dynamic_rendering.suballocation.*
